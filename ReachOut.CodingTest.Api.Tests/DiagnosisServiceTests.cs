﻿using ReachOut.CodingTest.Api.Repositories;
using ReachOut.CodingTest.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ReachOut.CodingTest.Api.Tests
{
    public class DiagnosisServiceTests
    {
        [Theory]
        [InlineData(new int[] { 1, 3 }, new int[] { 2, 2 })]
        [InlineData(new int[] { 3 }, new int[] { 2 })]
        [InlineData(new int[] { 3, 4 }, new int[] { 2, 2 })]
        [InlineData(new int[] { 1, 2, 3 }, new int[] { 2, 2, 2 })]
        public void SymptomsReturnCorrectIssueScores(int[] symptomIds, int[] expectedScores)
        {
            //Arrange
            var service = new DiagnosisService(new DiagnosisRepository());

            //Act
            var result = service.GetRelatedIssueScores(symptomIds);

            //Assert
            Assert.NotNull(expectedScores);
            Assert.Equal(expectedScores.Count(), result.SymptomRelatedIssueScores.Count());
            var scores = result.SymptomRelatedIssueScores.ToArray();
            for(int i = 0; i < scores.Length; i++)
            {
                Assert.Equal(expectedScores[i], scores[i].Score);
            }
            
            Assert.Equal(expectedScores.Sum(), result.TotalScore);

        }
    }
}

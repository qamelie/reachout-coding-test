using Microsoft.Practices.Unity;
using ReachOut.CodingTest.Api.Repositories;
using ReachOut.CodingTest.Api.Services;
using System.Web.Http;
using Unity.WebApi;

namespace ReachOut.CodingTest.Api
{
    public static class UnityConfig
    {
        public static IUnityContainer GetConfigurationContainer()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IDiagnosisService, DiagnosisService>();
            container.RegisterType<IDiagnosisRepository, DiagnosisRepository>();

            return container;
        }
    }
}
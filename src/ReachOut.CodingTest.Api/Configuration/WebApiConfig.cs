﻿using System.Web.Http;

namespace ReachOut.CodingTest.Api.Configuration
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
        }
    }
}

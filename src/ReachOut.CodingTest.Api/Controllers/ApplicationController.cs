﻿using System.Web.Http;

namespace ReachOut.CodingTest.Api.Controllers
{
    [RoutePrefix("api")]
    public class ApplicationController : ApiController
    {
        [HttpGet]
        [Route("helloworld")]
        public IHttpActionResult Hello()
        {
            return Ok(new { Message = "Hello world"});
        }
    }
}
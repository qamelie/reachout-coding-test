﻿using ReachOut.CodingTest.Api.Repositories;
using ReachOut.CodingTest.Api.Services;
using System.Web.Http;

namespace ReachOut.CodingTest.Api.Controllers
{
    [RoutePrefix("api/v1")]
    public class DiagnosisController : ApiController
    {
        private readonly IDiagnosisService _diagnosisService;
        private readonly IDiagnosisRepository _diagnosisRepository;

        public DiagnosisController(IDiagnosisService diagnosisService, IDiagnosisRepository diagnosisRepository)
        {
            _diagnosisService = diagnosisService;
            _diagnosisRepository = diagnosisRepository;
        }

        /// <summary>
        /// Returns related issue scores by symptomIds
        /// </summary>
        /// <param name="symptomId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("diagnosis/issuescores")]
        public IHttpActionResult GetIssueScores([FromUri] int[] symptomId)
        {
            if(symptomId == null)
            {
                return BadRequest("Requires at least one symptom");
            }

            var result = _diagnosisService.GetRelatedIssueScores(symptomId);
            return Ok(result);
        }

        /// <summary>
        /// Return all symptoms
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("symptoms")]
        public IHttpActionResult GetAllSymptoms()
        {
            var result = _diagnosisRepository.GetAllSymptoms();
            return Ok(result);
        }
        /// <summary>
        /// Returns all issues - for mapping Id returned in response
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("issues")]
        public IHttpActionResult GetAllIssues()
        {
            var result = _diagnosisRepository.GetAllIssues();
            return Ok(result);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReachOut.CodingTest.Api.Models;

namespace ReachOut.CodingTest.Api.Repositories
{
    public class DiagnosisRepository : IDiagnosisRepository
    {
        public IEnumerable<Issue> GetAllIssues()
        {
            return new List<Issue>
            {
                new Issue { Id =1, Name="Stress and body image, food or exercise", Description="I'm stressed about my body image, food or exercise" },
                new Issue { Id =2, Name="Feeling stressed and anxious", Description="I'm feeling stressed, anxious worried or downs" }
            };
        }

        public IEnumerable<SymptomIssueScore> GetAllSymptomIssueScores()
        {
            return new List<SymptomIssueScore>
            {
                new SymptomIssueScore { SymptomId=1, IssueId=2, Score=2 },
                new SymptomIssueScore { SymptomId=1, IssueId=1, Score=1 },
                new SymptomIssueScore { SymptomId=2, IssueId=1, Score=2 },
                new SymptomIssueScore { SymptomId=3, IssueId=1, Score=2 },
                new SymptomIssueScore { SymptomId=4, IssueId=1, Score=2 },
                new SymptomIssueScore { SymptomId=4, IssueId=2, Score=1 },
            };
        }

        public IEnumerable<Symptom> GetAllSymptoms()
        {
            return new List<Symptom>
            {
                new Symptom { Id =1, Name="Stressed", Description="I'm often feeling stressed" },
                new Symptom { Id =2, Name="Friends telling me about eating habits", Description="People tell me I have unhealthy eating habits" },
                new Symptom { Id =3, Name="Uncomfortable with body image", Description="I'm don't feel comfortable in my body" },
                new Symptom { Id =4, Name="Eating when upset", Description="I often eat when I am upset" }
            };
        }
    }
}
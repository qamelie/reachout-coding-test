﻿using System.Collections.Generic;
using ReachOut.CodingTest.Api.Models;

namespace ReachOut.CodingTest.Api.Repositories
{
    public interface IDiagnosisRepository
    {
        IEnumerable<Issue> GetAllIssues();
        IEnumerable<Symptom> GetAllSymptoms();
        IEnumerable<SymptomIssueScore> GetAllSymptomIssueScores();

    }
}
﻿namespace ReachOut.CodingTest.Api.Models
{
    public class Symptom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
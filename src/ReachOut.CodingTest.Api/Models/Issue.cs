﻿namespace ReachOut.CodingTest.Api.Models
{
    public class Issue
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReachOut.CodingTest.Api.Models
{
    public class DiagnosisResult
    {
        public IEnumerable<SymptomIssueScore> SymptomRelatedIssueScores { get; set; }
        public int TotalScore { get; set; }
    }
}
﻿namespace ReachOut.CodingTest.Api.Models
{
    public class SymptomIssueScore
    {
        public int SymptomId { get; set; }
        public int IssueId { get; set; }
        public int Score { get; set; }
    }
}
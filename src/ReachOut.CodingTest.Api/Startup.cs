﻿using System.Web.Http;
using Owin;
using ReachOut.CodingTest.Api.Configuration;
using Unity.WebApi;

namespace ReachOut.CodingTest.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();

            WebApiConfig.Register(config);
            config.DependencyResolver = new UnityDependencyResolver(UnityConfig.GetConfigurationContainer());
            appBuilder.UseWebApi(config);
        }
    }
}
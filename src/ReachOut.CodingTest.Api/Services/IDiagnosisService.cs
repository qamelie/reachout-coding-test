﻿using ReachOut.CodingTest.Api.Models;

namespace ReachOut.CodingTest.Api.Services
{
    public interface IDiagnosisService
    {
        DiagnosisResult GetRelatedIssueScores(int[] symptomIds);
    }
}

﻿using ReachOut.CodingTest.Api.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReachOut.CodingTest.Api.Models;

namespace ReachOut.CodingTest.Api.Services
{
    public class DiagnosisService : IDiagnosisService
    {
        private IDiagnosisRepository _diagnosisRepository;

        public DiagnosisService(IDiagnosisRepository diagnosisRepository)
        {
            _diagnosisRepository = diagnosisRepository;
        }

        public DiagnosisResult GetRelatedIssueScores(int[] symptomIds)
        {
            var result = _diagnosisRepository
                .GetAllSymptomIssueScores()
                .Where(x => symptomIds.Contains(x.SymptomId) && x.Score >= 2)
                .OrderByDescending(x => x.Score);

            return new DiagnosisResult
            {
                SymptomRelatedIssueScores = result,
                TotalScore = result.Sum(x => x.Score)
            };

        }
    }
}